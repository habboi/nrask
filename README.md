# Rask

## Changelog

|Version|Description|
|----|----|
|0.0.108|IO Tools for UTCode and using on_error and on_close in RMQ's class|
|0.0.107|raven can accept color and log format options|
|0.0.106|utcode float encode/decode fixed|
|0.0.105|main class has now utcode property as default|
|0.0.104|udatetime and motor is not required anymore, no more custom types in bencode and others small fixes|
|0.0.103|Better Announce|
|0.0.102|Announce is binding queue by headers|
|0.0.101|New rmq.ack. Future or bool is now accepted|
|0.0.100|Announce read queue's flags as **auto_delete**|
|0.0.99|Announce read queue's flags as **durable** and **exclusive**|
|0.0.98|Announce is now accepting future|
|0.0.97|Announce is now accepting queue settings as durable and exclusive flags|
|0.0.96|Better all meta|
