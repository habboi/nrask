from bencode import bdecode as decode, bencode as encode
from bencode.BTL import BTFailure

__all__ = [
    'BTFailure',
    'decode',
    'encode'
]
